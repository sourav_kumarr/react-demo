import React, { Component } from 'react';
import {
    AppRegistry
} from 'react-native';

import Login from './src/component/Pages/Login';
import Notification from './src/component/Pages/Notification';
import Messages from './src/component/Pages/Messages';
import Settings from './src/component/Pages/Setting';
import About from './src/component/Pages/Aboutus';
import Contact from './src/component/Pages/Contactus';
import Signup from './src/component/Pages/Signup';

import DrawerMenu from './src/component/Drawer/drawer-toolbar-android'
import App from './app'

import { DrawerNavigator, StackNavigator } from 'react-navigation';
import Forgot from "./src/component/Pages/Forgot";

const stackNavigator = StackNavigator({
     Notification: { screen: Notification },
     Messages: { screen: Messages},
    });



const drawerNavigation = DrawerNavigator({
    Home: {
        screen: App,
        navigationOptions: {
            header:false, //hide header if not needed so whole screen slide
        },

    },
    Notification: { screen: Notification },
    Messages: { screen: Messages},
    Settings: { screen: Settings},
    About: { screen: About},
    Contact: { screen: Contact}
    }, {
    contentComponent: DrawerMenu,
    contentOptions: {
        activeTintColor: '#e91e63',
        style: {
            flex: 1,
            paddingTop: 15,
        }
    }
});

 export const Root = StackNavigator({
    Login:{
        screen:Login
    },
    Drawer:{
        screen:drawerNavigation
    },
     Signup:{
         screen:Signup
     },
     Forgot:{
         screen:Forgot
     }


 }, {
    headerMode: 'none'
});
console.disableYellowBox = true;

AppRegistry.registerComponent("AwesomeProject" , () => Root );