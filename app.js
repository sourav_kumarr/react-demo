import React, { Component } from 'react';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import {
  AppRegistry,
  StyleSheet,
  Text,
  StatusBar,
  View,
  Alert
} from 'react-native';
import { Navigator, NativeModules } from 'react-native';

import { COLOR, ThemeProvider } from 'react-native-material-ui';
import { Toolbar, BottomNavigation, Icon } from 'react-native-material-ui';
import Container from './src/component/Container';

import { TabRouter,StackNavigator } from 'react-navigation';

import RecommendsView from './src/component/Pages/RecommendsView';
import FriendsView from './src/component/Pages/Friends';
import PostsView from './src/component/Pages/Posts';
import {Tabs} from "./src/component/Container/router";

// import ProfileView from './Contents/profile';
// import MapView from './Contents/map';
// import ChatView from './Contents/chat';

const uiTheme = {
  palette: {
    primaryColor: '#d75033',
    accentColor: COLOR.pink500,
  },
  toolbar: {
    container: {
      height: 70,
      paddingTop: 20
    }
  }

};
mapStateToProps = (state, props) => ({
    navigation: this.props.navigation
});
const TabRoute = TabRouter({
  Recommends: {screen:RecommendsView},
  Friends: { screen: FriendsView},
  Posts: { screen: PostsView},
    }, {
    initialRouteName: 'Recommends',
    swipeEnabled:true
  }
);


class TabContentNavigator extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      active: props.value.active,
    };
  }

  //this method will not get called first time
  componentWillReceiveProps(newProps){
    this.setState({
      active: newProps.value.active,
    }); 
  }

  render() {
    const Component = TabRoute.getComponentForRouteName(this.state.active);
    return <Component/>;
  }
}

class App extends Component {
  constructor(props, context) {
    super(props, context);
    
    this.state = {
      active: 'Friends',
    };
  }

  static navigationOptions = {
    title: 'Menu',
  };

  navigate() {
    this.props.navigation.navigate('DrawerOpen'); // open drawer
  }
  componentDidMount() {
        // Alert.alert('props',''+this.props.navigation);
    }

  render() {
    return (
      <ThemeProvider uiTheme={uiTheme}>
        <Container>
          <StatusBar backgroundColor="#d75033" translucent />

          <Toolbar
            leftElement="menu"
            centerElement={this.state.active}
            onLeftElementPress={() => this.navigate()}
          />
          <Tabs/>
          {/*<TabContentNavigator value={this.state} key={this.state} />

          <BottomNavigation active={this.state.active}
            hidden={false}
             style={{container:{ position:'absolute',bottom:0,left:0,right:0}} }>
            <BottomNavigation.Action
              key="Recommends"
              icon="today"
              label="Recommends"
              style={{ container: { minWidth: responsiveWidth(33),width:responsiveWidth(33) } }}
              onPress={() => this.setState({ active: 'Recommends' })}
            />
            <BottomNavigation.Action
              key="Friends"
              icon="person"
              label="Friends"
              style={{ container: { minWidth: responsiveWidth(33),width:responsiveWidth(33) } }}
              onPress={() => {
                this.setState({ active: 'Friends' });
              }}
            />
            <BottomNavigation.Action
                  key="Posts"
                  icon="person"
                  label="Posts"
                  style={{ container: { minWidth: responsiveWidth(33),width:responsiveWidth(33) } }}
                  onPress={() => {
                      this.setState({ active: 'Posts' });
                  }}
              />
          </BottomNavigation>*/}

        </Container>
      </ThemeProvider>
    );
  }
}
export default App;

