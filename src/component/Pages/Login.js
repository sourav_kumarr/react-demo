import React, { Component } from 'react';
import { Icon,Button,Divider,Row,Col,Grid } from 'react-native-elements';
import * as Globals from '../Helper/Globals';
import * as Progress from 'react-native-progress';

import {
    ScrollView,
    Text,
    TextInput,
    View,
    Alert,
    StyleSheet,
    Image,
    AlertIOS
} from 'react-native';
import DialogManager, { ScaleAnimation, DialogContent } from 'react-native-dialog-component'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import ResponsiveImage from 'react-native-responsive-image';
let SharedPreferences = require('react-native-shared-preferences');

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            UserName: '',
            UserPassword: '',
            isLogged:false

        };

    }
    render() {
        return (
            <ScrollView>
            <View
                style={styles.container}>

                <ResponsiveImage  style={styles.profile_img} source={require('../images/profile.jpg')} initWidth="100" initHeight="100"/>
                <View style={styles.textBox}>
                    <Icon
                        name='mail'
                        type='material'
                        color='#fff'
                        size={responsiveHeight(3)}
                    />
                    <TextInput underlineColorAndroid='transparent' placeholderTextColor='#fff'
                               onChangeText={(text) => this.setState({UserName:text})}
                               value={this.state.UserName} placeholder='Username'
                               style={{width:responsiveWidth(80),flex:2,color:'#fff',height:responsiveHeight(10)}}/>
                </View>

                <View style={[styles.textBox,styles.passwordBox]}>
                    {/*<Image source={require('./images/back.jpg')} style={{width:40,height:30}}/>*/}
                    <Icon
                        name='lock'
                        type='material'
                        color='#fff'
                        size={responsiveHeight(3)}
                    />
                    <TextInput underlineColorAndroid='transparent' placeholderTextColor='#fff' placeholder='Password'
                               onChangeText={(text) => this.setState({UserPassword:text})}
                               value={this.state.UserPassword}
                               secureTextEntry={true}
                               style={{width:responsiveWidth(80),flex:2,borderBottom:0,color:'#fff',height:responsiveHeight(10)}}/>
                </View>
                <View style={{marginTop:50}} />

                <Button
                    raised
                    buttonStyle={styles.buttonStyle}
                    textStyle={{textAlign: 'center',color:'white',fontSize:30}}
                    title={`Login`}
                    onPress = {() => this.onLoginPress()}
                />
                <Text style={styles.forgetText} onPress = {() => this.onForgot()}>Forget Password ?</Text>
                <View style={styles.orContainer}>
                    <Divider style={styles.hrLine} />
                    <Text style={styles.orText}>OR</Text>
                    <Divider style={styles.hrLine} />
                </View>
                <Text style={styles.signUpText} onPress = {() => this.onSignUp()}>Sign Up</Text>

            </View>
            </ScrollView>
        )
    }

    componentDidMount(){
      console.log('componentDidMount is called');
      Alert.alert('props',''+this.props.navigation);
        SharedPreferences.getItem(Globals.USER_NAME, function(value){
            console.log(value);
            if(value) {
                // this.props.navigation.navigate('Drawer');
            }
        });

    }

    onSignUp() {
        this.props.navigation.navigate('Signup');
    }

    onForgot() {
        this.props.navigation.navigate('Forgot');
    }

    onLoginPress() {
        // Alert.alert("Login","Logged in successfully "+this.props);
        // this.props.navigation.navigate("Drawer");
        let user_name = this.state.UserName;
        let user_password = this.state.UserPassword;
        if(user_name ==='' || user_password ==='') {
         Alert.alert("Error","Please enter all fields");
         return;
        }
        DialogManager.show({
            titleAlign: 'left',
            animationDuration: 200,
            width:responsiveWidth(80),
            height:responsiveHeight(10),
            ScaleAnimation: new ScaleAnimation(),
            children: (
                <DialogContent>
                    <View style={{display:'flex',flexDirection:'row'}}>
                        <Progress.CircleSnail  size={responsiveHeight(7)} indeterminate={true} showsText={true} thickness={3} color={['#d75033','#d75033','#d75033']}/>
                        <Text style={{marginTop:responsiveHeight(2),marginLeft:responsiveWidth(4),fontSize:responsiveFontSize(2)}}>Loading ....</Text>

                    </View>
                </DialogContent>
            ),
        }, () => {
            console.log('callback - show');
        });

        let url = Globals.BASE_URL+Globals.LOGIN_URL;

        fetch(url, {method: "POST",
            headers: {'Accept': 'application/json',
                'Content-Type': 'application/json'},
            body:JSON.stringify({email:user_name,password:user_password,type:'user'}) })
            .then((response) => response.json())
            .then((responseData) => {
                DialogManager.dismissAll(() => {
                    console.log('callback - dismiss all');
                });
                let status = responseData.Status;
                if(status) {
                    let data = responseData.data;
                    let name = data.user_name;
                    let user_id = data.user_id;
                    SharedPreferences.setItem(Globals.USER_NAME,name);
                    SharedPreferences.setItem(Globals.USER_EMAIL,user_name);
                    SharedPreferences.setItem(Globals.USER_ID,''+user_id);

                    this.props.navigation.navigate('Drawer',{navigation:this.props.navigation});
                    return false;
                }
                Alert.alert("Login", responseData.Message);

            }).done();

    };
}



const styles = StyleSheet.create({
    bigblue: {
        color: 'blue',
        fontWeight: 'bold',
        fontSize: 30,
    },
    red: {
        color: 'red',
    },
    loginContainter:{
      alignItems:'center',
      justifyContent:'center',
      marginTop:responsiveHeight(30)

    },
    loginText:{
        fontSize:responsiveFontSize(30),

    },
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#d75033',
        alignItems: 'center',
        height:responsiveHeight(100)
    },
    profile_img:{
        marginTop:responsiveHeight(2),
        tintColor:'white'
    },
    textBox:{
        alignItems:'center',
        color:'#fff',
        padding:2,
        borderColor:'white',
        borderWidth:3,
        borderRadius:10,
        marginTop:responsiveHeight(3),
        width:responsiveWidth(70),
        flexDirection:'row',
        height:responsiveHeight(7)
    },
    passwordBox:{
        marginTop:responsiveHeight(5)
    },
    buttonStyle:{
        backgroundColor: '#8c231f',
        width:responsiveWidth(70),
        padding:10
    },
    forgetText:{
        marginTop:responsiveHeight(7),
        marginBottom:responsiveHeight(5),
        color:'#fff',
        fontSize:responsiveFontSize(1.5)
    },
    orContainer:{
        flexDirection:'row',
        width:responsiveWidth(70),
        marginBottom:responsiveWidth(5),
    },
    orText:{
        color:'#fff',
        fontSize:responsiveFontSize(1.5),
        paddingLeft:15,
        paddingRight:15,
        marginTop:-13,
        fontWeight:'600'

    },
    hrLine:{
        height:3,
        backgroundColor:'#fff',
        flex:2
    },
    signUpText:{
        color:'#fff',
        fontSize:responsiveFontSize(1.5),
        borderBottomWidth:1,
        borderBottomColor:'#fff',
        marginBottom:responsiveHeight(3)
    }


});
