import React, {Component} from 'react';
import {Icon, Button, Divider, Row, Col, Grid} from 'react-native-elements';
import * as Globals from '../Helper/Globals';
import * as Progress from 'react-native-progress';

import {
    ScrollView,
    Text,
    TextInput,
    View,
    Alert,
    StyleSheet,
    Image,
    AlertIOS
} from 'react-native';
import DialogManager, {ScaleAnimation, DialogContent} from 'react-native-dialog-component'
import {responsiveHeight, responsiveWidth, responsiveFontSize} from 'react-native-responsive-dimensions';
import ResponsiveImage from 'react-native-responsive-image';
import renderIf from '../Helper/Renderif';
let statee = null;

export default class Forgot extends Component {

    constructor(props) {
        super(props);
        this.state = {
            UserEmail: '',
            UserOtp: '',
            UserPassword: '',
            UserConfirPassword: '',
            isOtpSend: false

        };
        statee = this;

    }

    render() {
        return (
            <ScrollView>
                <View
                    style={styles.container}>

                    <ResponsiveImage style={styles.profile_img} source={require('../images/profile.jpg')}
                                     initWidth="100" initHeight="100"/>
                    <View style={styles.textBox}>
                        <Icon
                            name='mail'
                            type='material'
                            color='#fff'
                            size={responsiveHeight(3)}
                        />
                        <TextInput underlineColorAndroid='transparent' placeholderTextColor='#fff'
                                   onChangeText={(text) => this.setState({UserEmail: text})}
                                   value={this.state.UserEmail} placeholder='Enter user email'
                                   style={{
                                       width: responsiveWidth(80),
                                       flex: 2,
                                       color: '#fff',
                                       height: responsiveHeight(10)
                                   }}/>
                    </View>
                    {renderIf(this.state.isOtpSend,
                    <View style={styles.textBoxContainer}>
                        <View style={[styles.textBox]}>
                            <Icon
                                name='lock'
                                type='material'
                                color='#fff'
                                size={responsiveHeight(3)}
                            />
                            <TextInput underlineColorAndroid='transparent' placeholderTextColor='#fff'
                                       onChangeText={(text) => this.setState({UserOtp: text})}
                                       value={this.state.UserOtp} placeholder='Enter one time password'
                                       style={{
                                           width: responsiveWidth(80),
                                           flex: 2,
                                           color: '#fff',
                                           height: responsiveHeight(10)
                                       }}/>
                        </View>


                        <View style={styles.textBox}>
                            <Icon
                                name='lock'
                                type='material'
                                color='#fff'
                                size={responsiveHeight(3)}
                            />
                            <TextInput underlineColorAndroid='transparent' placeholderTextColor='#fff'
                                       onChangeText={(text) => this.setState({UserPassword: text})}
                                       value={this.state.UserPassword} placeholder='Enter password'
                                       style={{
                                           width: responsiveWidth(80),
                                           flex: 2,
                                           color: '#fff',
                                           height: responsiveHeight(10)
                                       }}/>
                        </View>
                        <View style={styles.textBox}>
                            <Icon
                                name='lock'
                                type='material'
                                color='#fff'
                                size={responsiveHeight(3)}
                            />
                            <TextInput underlineColorAndroid='transparent' placeholderTextColor='#fff'
                                       onChangeText={(text) => this.setState({UserConfirPassword: text})}
                                       value={this.state.UserConfirPassword} placeholder='Confirm password'
                                       style={{
                                           width: responsiveWidth(80),
                                           flex: 2,
                                           color: '#fff',
                                           height: responsiveHeight(10)
                                       }}/>
                        </View>
                    </View>)
                    }

                    <View style={{marginTop: 10}}/>
                    <View style={[styles.btnContainer]}>
                        <Button
                            raised
                            buttonStyle={styles.buttonStyle}
                            textStyle={{textAlign: 'center', color: 'white', fontSize: 15}}
                            title={`Send Otp`}
                            onPress={() => this.onOtpSend()}
                        />
                        <Button
                            raised
                            buttonStyle={styles.buttonStyle}
                            textStyle={{textAlign: 'center', color: 'white', fontSize: 15}}
                            title={`Update Password`}
                            onPress={() => this.onUpdatePassword()}
                        />
                    </View>
                </View>
            </ScrollView>
        )

    }

    onOtpSend() {
        let user_email = this.state.UserEmail;
        if(user_email ==='' ) {
            Alert.alert("Error","Please enter all fields");
            return;
        }
        DialogManager.show({
            titleAlign: 'left',
            animationDuration: 200,
            width:responsiveWidth(80),
            height:responsiveHeight(10),
            ScaleAnimation: new ScaleAnimation(),
            children: (
                <DialogContent>
                    <View style={{display:'flex',flexDirection:'row'}}>
                        <Progress.CircleSnail  size={responsiveHeight(7)} indeterminate={true} showsText={true} thickness={3} color={['#d75033','#d75033','#d75033']}/>
                        <Text style={{marginTop:responsiveHeight(2),marginLeft:responsiveWidth(4),fontSize:responsiveFontSize(2)}}>Loading ....</Text>

                    </View>
                </DialogContent>
            ),
        }, () => {
            console.log('callback - show');
        });
        let url = Globals.BASE_URL+Globals.OTP_URL;

        fetch(url, {method: "POST",
            headers: {'Accept': 'application/json',
                'Content-Type': 'application/json'},
            body:JSON.stringify({email:user_email}) })
            .then((response) => response.json())
            .then((responseData) => {
                DialogManager.dismissAll(() => {
                    console.log('callback - dismiss all');
                });

                let status = responseData.Status;
                let message = responseData.Message;
                if(status) {
                    this.setState({isOtpSend:true});
                    return false;
                }
                Alert.alert("Error", message);

            }).done();
    }

    onUpdatePassword() {

        let user_email = this.state.UserEmail;
        let user_password = this.state.UserPassword;
        let user_confirm = this.state.UserConfirPassword;
        let user_otp = this.state.UserOtp;

        if(user_email ==='' || user_password === '' || user_confirm === '' || user_otp === '') {
            Alert.alert("Error","Please enter all fields");
            return;
        }
        else if(user_password!== user_confirm) {
            Alert.alert("Error","Password does not match");
            return;
        }

        DialogManager.show({
            titleAlign: 'left',
            animationDuration: 200,
            width:responsiveWidth(80),
            height:responsiveHeight(10),
            ScaleAnimation: new ScaleAnimation(),
            children: (
                <DialogContent>
                    <View style={{display:'flex',flexDirection:'row'}}>
                        <Progress.CircleSnail  size={responsiveHeight(7)} indeterminate={true} showsText={true} thickness={3} color={['#d75033','#d75033','#d75033']}/>
                        <Text style={{marginTop:responsiveHeight(2),marginLeft:responsiveWidth(4),fontSize:responsiveFontSize(2)}}>Loading ....</Text>

                    </View>
                </DialogContent>
            ),
        }, () => {
            console.log('callback - show');
        });
        let url = Globals.BASE_URL+Globals.VERIFY_OTP_URL;

        fetch(url, {method: "POST",
            headers: {'Accept': 'application/json',
                'Content-Type': 'application/json'},
            body:JSON.stringify({email:user_email,otp:user_otp,password:user_password}) })
            .then((response) => response.json())
            .then((responseData) => {
                DialogManager.dismissAll(() => {
                    console.log('callback - dismiss all');
                });
                let status = responseData.Status;
                let message = responseData.Message;
                if(status) {
                    this.props.navigation.navigate('Login');
                    return false;
                }
                Alert.alert("Error", message);

            }).done();
    }
}
const styles = StyleSheet.create({
    bigblue: {
        color: 'blue',
        fontWeight: 'bold',
        fontSize: 30,
    },
    red: {
        color: 'red',
    },
    loginContainter: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: responsiveHeight(30)

    },
    loginText: {
        fontSize: responsiveFontSize(30),

    },
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#d75033',
        alignItems: 'center',
        height: responsiveHeight(100)
    },
    profile_img: {
        marginTop: responsiveHeight(2),
        tintColor: 'white'
    },
    textBox: {
        alignItems: 'center',
        color: '#fff',
        padding: 2,
        borderColor: 'white',
        borderWidth: 3,
        borderRadius: 10,
        marginTop: responsiveHeight(3),
        width: responsiveWidth(87),
        flexDirection: 'row',
        height: responsiveHeight(7)
    },
    passwordBox: {
        marginTop: responsiveHeight(5)
    },
    buttonStyle: {
        backgroundColor: '#8c231f',
        width: responsiveWidth(42),
        padding: responsiveWidth(2),

    },
    forgetText: {
        marginTop: responsiveHeight(7),
        marginBottom: responsiveHeight(5),
        color: '#fff',
        fontSize: responsiveFontSize(1.5)
    },
    orContainer: {
        flexDirection: 'row',
        width: responsiveWidth(70),
        marginBottom: responsiveWidth(5),
    },
    orText: {
        color: '#fff',
        fontSize: responsiveFontSize(1.5),
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: -13,
        fontWeight: '600'

    },
    hrLine: {
        height: 3,
        backgroundColor: '#fff',
        flex: 2
    },
    signUpText: {
        color: '#fff',
        fontSize: responsiveFontSize(1.5),
        borderBottomWidth: 1,
        borderBottomColor: '#fff',
        marginBottom: responsiveHeight(3)
    },
    btnContainer: {
        display: 'flex',
        flexDirection: 'row',
        marginTop:responsiveHeight(2)
    },
    textBoxContainer:{
        display:'flex',
        flexDirection: 'column',
        backgroundColor: '#d75033',
        alignItems: 'center',
    }


});

