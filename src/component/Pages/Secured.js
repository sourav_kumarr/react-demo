/**
 * Created by Sourav on 9/1/2017.
 */


import React, { Component } from 'react';
import {
    ScrollView,
    Text,
    View,
    Button,
    Alert
} from 'react-native';

export default class Secured extends Component {


    goBack() {
      this.props.navigation.navigate("Login");
    }

    render() {
        return (
            <ScrollView style={{padding: 20}}>
                <Text
                    style={{fontSize: 27}}>
                    Welcome
                </Text>
                <View style={{margin:20}} />
                <Button
                    onPress={() => this.goBack()}
                    title="Logout"
                />
            </ScrollView>
        )
    }
}