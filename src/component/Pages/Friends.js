import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    StatusBar,
    View,
    Alert
} from 'react-native';
import * as Globals from '../Helper/Globals';
import {responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {Card} from 'react-native-material-ui';
import { Icon} from 'react-native-elements';

let GiftedListView = require('react-native-gifted-listview');
let SharedPreferences = require('react-native-shared-preferences');

let statee;

export default class RecommendsView extends Component {
    constructor(props) {
        super(props);
        statee = this;
        this.state = {
            user_id: '',
        };
    }

    componentDidMount() {
        SharedPreferences.getItem(Globals.USER_ID, function(value){
            console.log(value);
            if(value) {
                statee.setState({user_id:value});
            }
        });
    }

    /**
     * Will be called when refreshing
     * Should be replaced by your own logic
     * @param {number} page Requested page to fetch
     * @param {function} callback Should pass the rows
     * @param {object} options Inform if first load
     */

    _onFetch(page = 1, callback, options) {

        let url = Globals.BASE_URL + Globals.FRIEND_URL;

        SharedPreferences.getItem(Globals.USER_ID, function(value){
            console.log(value);
            if(value) {
                fetch(url, {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body:JSON.stringify({user_id:value})
                })
                    .then((response) => response.json())
                    .then((responseData) => {

                        let status = responseData.Status;
                        let message = responseData.Message;

                        if(status) {
                            // this.state.setState({dataSource:responseData.data});
                            callback(responseData.data);
                            return true;
                        }

                        Alert.alert("Error",message );

                    }).done();
            }
        });

    }

    /**
     * Render a row
     * @param {object} rowData Row data
     */

    _renderRowView(rowData) {


        return (
            <Card>
            <View style={styles.listContainer}>
                <Text style ={styles.profileImg}>{rowData.user_name.charAt(0)}</Text>
                <View style={styles.userInfo}>
                    <Text style={styles.userName}>{rowData.user_name}</Text>
                    <Text style={{marginTop:responsiveHeight(1)}}>{rowData.user_email}</Text>
                </View>
            </View>
            </Card>
        );
    }

    render() {
        return (
            <GiftedListView
                rowView={this._renderRowView}
                onFetch={this._onFetch}
                firstLoader={true} // display a loader for the first fetching
                pagination={false} // enable infinite scrolling using touch to load more
                refreshable={true} // enable pull-to-refresh for iOS and touch-to-refresh for Android
                withSections={false} // enable sections
                customStyles={{
                    paginationView: {
                        backgroundColor: '#eee',
                    },
                    height:responsiveHeight(80)
                }}

                refreshableTintColor="blue"
            />
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    header: {
        backgroundColor: '#455A64',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    listContainer:{
        display:'flex',
        flexDirection:'row',
        padding:responsiveWidth(5)
    },
    profileImg:{
        width:responsiveHeight(7),
        height:responsiveHeight(7),
        borderRadius:responsiveHeight(100),
        backgroundColor:'#d75033',
        color:'#ffffff',
        textAlign:'center',
        paddingTop:15,
        fontWeight:'bold',

    },
    userInfo:{
        display:'flex',
        flexDirection:'column',
        marginLeft:responsiveWidth(4),
        marginTop:responsiveWidth(1)

    },
    userName:{
        fontWeight:'bold',
        letterSpacing:10
    }

});