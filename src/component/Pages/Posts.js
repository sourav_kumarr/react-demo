import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    StatusBar,
    View,
    Alert,
    Image,
    ListView,
    TouchableHighlight
} from 'react-native';

import DialogManager, {ScaleAnimation, DialogContent} from 'react-native-dialog-component'
import * as Globals from '../Helper/Globals';
import {responsiveHeight, responsiveWidth} from 'react-native-responsive-dimensions';
import {Card} from 'react-native-material-ui';
import { Icon} from 'react-native-elements';


let GiftedListView = require('react-native-gifted-listview');
let statee;
let navigation;


export default class PostsView extends Component {
    constructor(props) {
        super(props);

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(['row 1', 'row 2']),
        };
        statee = this;
        navigation = this.props.navigation;
    }

    /**
     * When a row is touched
     * @param {object} rowData Row data
     */

    listItemClicked(rowData) {

        navigation.navigate('PostDesc',{...rowData});

    }

    /**
     * Will be called when refreshing
     * Should be replaced by your own logic
     * @param {number} page Requested page to fetch
     * @param {function} callback Should pass the rows
     * @param {object} options Inform if first load
     */
    _onFetch(page = 1, callback, options) {
        /*setTimeout(() => {
            let rows = ['row '+((page - 1) * 3 + 1), 'row '+((page - 1) * 3 + 2), 'row '+((page - 1) * 3 + 3)];
            if (page === 3) {
                callback(rows, {
                    allLoaded: true, // the end of the list is reached
                });
            } else {
                callback(rows);
            }
        }, 1000); */

        let url = Globals.BASE_URL + Globals.POSTS_URL;

        fetch(url, {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then((response) => response.json())
            .then((responseData) => {
                DialogManager.dismissAll(() => {
                    console.log('callback - dismiss all');
                });
                let status = responseData.Status;
                let message = responseData.Message;

                if(status) {
                    // this.state.setState({dataSource:responseData.data});
                    callback(responseData.data);
                    return true;
                }

                Alert.alert("Error",message );

            }).done();
    }

    /**
     * Render a row
     * @param {object} rowData Row data
     */
    _renderRowView(rowData) {


        return (
            <Card onPress={() => statee.listItemClicked(rowData)}>
                <Image
                    source={{uri: Globals.IMAGE_URL+rowData.post_image}}
                    resizeMode="stretch"
                    style={styles.fitImageWithSize}
                />
                <View style={styles.footerContainer}>
                    <Text>{rowData.post_title}</Text>
                    <View style={styles.footerRightContent}>
                        <Icon
                            name='favorite'
                            type='material'
                            color='#000'
                            size={responsiveHeight(3)}
                            style={styles.footerRightItems}
                        />
                        <Text style={styles.footerRightItems}>{rowData.post_likes}</Text>

                        <Icon
                            name='message'
                            type='material'
                            color='#000'
                            size={responsiveHeight(3)}
                            style={styles.footerRightItems}
                        />
                        <Text style={styles.footerRightItems}>{rowData.post_comments}</Text>

                    </View>
                </View>
            </Card>
        );
    }

    render() {
        return (
            <GiftedListView
                rowView={this._renderRowView}
                onFetch={this._onFetch}
                firstLoader={true} // display a loader for the first fetching
                pagination={false} // enable infinite scrolling using touch to load more
                refreshable={true} // enable pull-to-refresh for iOS and touch-to-refresh for Android
                withSections={false} // enable sections
                customStyles={{
                    paginationView: {
                        backgroundColor: '#eee',
                    },
                    height:responsiveHeight(80)
                }}

                refreshableTintColor="blue"
            />

        );
    }
};

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection:'column',
        backgroundColor: '#F5FCFF',
    },
    header: {
        backgroundColor: '#455A64',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    row: {
        padding: 10,
        height: 44,
        borderBottomWidth:1,
        borderBottomColor:'#d75033'
    },

    fitImageWithSize: {
        height: responsiveHeight(30),
        width: responsiveWidth(100),
        padding: responsiveHeight(5)
    },
    footerContainer: {
        padding: responsiveHeight(2),
        width: responsiveWidth(100),
        display: 'flex',
        flexDirection: 'row'
    },
    footerRightContent: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        paddingRight: responsiveWidth(5)
    },
    footerRightItems:{
        paddingRight:responsiveWidth(2)
    }

});