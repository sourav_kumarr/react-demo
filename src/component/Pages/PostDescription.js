import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    StatusBar,
    View,
    Alert,
    Image,
    ListView,
    TouchableHighlight,
    TouchableOpacity,
    ScrollView,
    Button,
    TextInput
} from 'react-native';
import DialogManager, {ScaleAnimation, DialogContent} from 'react-native-dialog-component'
import * as Globals from '../Helper/Globals';
import * as Progress from 'react-native-progress';
import {responsiveHeight, responsiveWidth, responsiveFontSize} from 'react-native-responsive-dimensions';
import FitImage from 'react-native-fit-image';
import {Card} from 'react-native-material-ui';
import { Icon} from 'react-native-elements';
let GiftedListView = require('react-native-gifted-listview');
let statee;

let SharedPreferences = require('react-native-shared-preferences');

export default class PostsDescription extends Component {
    constructor(props, context) {
        super(props, context);
        statee = this;

        const {post_id,post_title,post_image,post_likes,post_comments,post_desc} = this.props.navigation.state.params;
        statee.state = {
            commText:'',
            post_id:post_id,
            post_likes:post_likes,
            post_comments:post_comments,
            user_id:'',
            post_desc:post_desc,
            post_title:post_title,
            post_image:post_image
        };

    }

    _onFetch(page = 1, callback, options) {

        let url = Globals.BASE_URL + Globals.COMMENT_URL+statee.state.post_id;

        fetch(url, {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then((response) => response.json())
          .then((responseData) => {
                DialogManager.dismissAll(() => {
                    console.log('callback - dismiss all');
                });
                let status = responseData.Status;
                let message = responseData.Message;
                let data = [];
                if(status) {
                    // this.state.setState({dataSource:responseData.data});
                    data = responseData.data;
                }
                callback(data);
                console.log(' error --- '+message);
            }).done();
    }

    _renderRowView(rowData) {
        return (
            <Card >
                <View style={{padding:responsiveHeight(1)}}>
                    <Text style={styles.commentText}>{rowData.comm_text}</Text>
                    <Text style={{marginTop:responsiveHeight(1)}}>{rowData.user_name}</Text>
                    <Text style={{fontWeight:'bold'}}>{rowData.user_email}</Text>
                </View>

            </Card>
        );
    }


    render(){
         // statee.setState({post_id:post_id,post_likes:post_likes,post_comments:post_comments});
        return(
           <ScrollView>
            <View style={styles.container}>
                <TouchableOpacity style={styles.backButton} onPress = {() => this.onBackPressed()}>
                    <View>
                    <Icon
                        name='arrow-back'
                        type='material'
                        color='#fff'
                        // size={responsiveHeight(3)}
                        style={styles.footerRightItems}
                    />
                    </View>
                </TouchableOpacity>
            <View style={{zIndex:0,borderWidth:1,padding:1,borderColor:'#eee'}}>
                <Image
                    source={{uri: Globals.IMAGE_URL+this.state.post_image}}
                    resizeMode="stretch"
                    style={styles.fitImageWithSize}
                />
                <View style={styles.footerContainer}>
                    <Text>{this.state.post_title}</Text>
                    <View style={styles.footerRightContent}>
                        <TouchableOpacity>
                        <Icon
                            name='favorite'
                            type='material'
                            color='#000'
                            size={responsiveHeight(3)}
                            style={styles.footerRightItems}
                            onPress = {() => this.onLikePressed()}
                        />
                        </TouchableOpacity>
                        <Text style={styles.footerRightItems}>{this.state.post_likes}</Text>

                        <Icon
                            name='message'
                            type='material'
                            color='#000'
                            size={responsiveHeight(3)}
                            style={styles.footerRightItems}
                        />
                        <Text style={styles.footerRightItems}>{this.state.post_comments}</Text>

                    </View>
                </View>
            </View>
                <View style={styles.descContainer}>
                <Text style={styles.descHeader}>Description : </Text>
                <Text>{this.state.post_desc}</Text>
                <Text style ={styles.descHeader} >Comments : </Text>
                    <View style={styles.commentContainer} >
                        <TextInput placeholder='Enter comment here' onChangeText={(text) => this.setState({commText:text})} style={{width:responsiveWidth(87)}}/>
                        <TouchableOpacity style={styles.commentBtn} onPress = {() => this.onCommentPress()}>

                            <Icon
                            name='add'
                            type='material'
                            color='#fff'
                            containerStyle={{marginTop:responsiveWidth(1)}}
                            />
                        </TouchableOpacity>
                    </View>
                    <GiftedListView
                        rowView={this._renderRowView}
                        onFetch={this._onFetch}
                        firstLoader={true} // display a loader for the first fetching
                        pagination={false} // enable infinite scrolling using touch to load more
                        refreshable={true} // enable pull-to-refresh for iOS and touch-to-refresh for Android
                        withSections={false} // enable sections
                        customStyles={{
                            paginationView: {
                                backgroundColor: '#eee',
                            },
                        }}
                        refreshableTintColor="blue"
                    />
                </View>


            </View>
           </ScrollView>
        );
    }

     componentDidMount() {
         SharedPreferences.getItem(Globals.USER_ID, function(value){
             console.log(value);
             if(value) {
                 // this.props.navigation.navigate('Drawer');
                 statee.setState({user_id:value});
             }
         });
         // Alert.alert('comments','dd '+statee.state.post_comments);
     }

      onLikePressed() {
        
          let url = Globals.BASE_URL + Globals.POST_LIKE_URL;
          let count = parseInt(statee.state.post_likes)+1;
          fetch(url, {
              method: "POST",
              headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
              },
              body:JSON.stringify({post_id:statee.state.post_id,user_id:statee.state.user_id,like_count:count})
          }).then((response) => response.json())
              .then((responseData) => {
                  /*DialogManager.dismissAll(() => {
                      console.log('callback - dismiss all');
                  });*/
                  let status = responseData.Status;
                  let message = responseData.Message;

                  if(status) {
                      // this.state.setState({dataSource:responseData.data});
                      Alert.alert('Success','Like is updated');
                      statee.setState({post_likes:count});
                      return false;
                  }
                  Alert.alert('Error',message);
                  console.log(' error --- '+message);
              }).done();
      }
      onBackPressed() {
       // Alert.alert('props','hello');

       this.props.navigation.navigate('Posts');
     }
     onCommentPress() {
        // Alert.alert('comment','pressed item '+this.state.commText);
        let commText = this.state.commText;
        if(commText === '') {
            Alert.alert('Comment','Please enter comment before post ');
            return false;
        }
         let url = Globals.BASE_URL + Globals.POST_COMMENT_URL;
         let count = parseInt(statee.state.post_comments)+1;
         fetch(url, {
             method: "POST",
             headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'application/json'
             },
             body:JSON.stringify({post_id:statee.state.post_id,user_id:statee.state.user_id,
             comment_count:count,comment_text:commText})
         }).then((response) => response.json())
             .then((responseData) => {
                 /*DialogManager.dismissAll(() => {
                     console.log('callback - dismiss all');
                 });*/
                 let status = responseData.Status;
                 let message = responseData.Message;

                 if(status) {
                     // this.state.setState({dataSource:responseData.data});
                     Alert.alert('Success','Comment added please refresh to see data');
                     statee.setState({post_comments:count});
                     return false;
                 }
                 Alert.alert('Error',message);
                 console.log(' error --- '+message);
             }).done();
     }
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection:'column',
        backgroundColor: '#F5FCFF',
    },
    fitImageWithSize: {
        height: responsiveHeight(30),
        width: responsiveWidth(100),
        // padding: responsiveHeight(5)
    },
    footerContainer: {
        padding: responsiveHeight(2),
        width: responsiveWidth(100),
        display: 'flex',
        flexDirection: 'row'
    },
    footerRightContent: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        paddingRight: responsiveWidth(5)
    },
    footerRightItems:{
        paddingRight:responsiveWidth(2)
    },
    descHeader:{
      fontWeight:'bold',
      marginTop:responsiveHeight(2)
    },
    descContainer:{
        padding:responsiveWidth(2),
        zIndex:0
    },
    backButton:{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height:responsiveWidth(13),
        width:responsiveWidth(13),
        // borderRadius:responsiveWidth(30),
        position:'absolute',
        backgroundColor:'#d75033',
        top:responsiveHeight(24),
        right:responsiveWidth(3),
        borderRadius:responsiveWidth(15),
        paddingLeft:responsiveWidth(1),
        zIndex:1000
    },
    commentContainer:{
        display:'flex',
        flexDirection:'row',
        width:responsiveWidth(100)
    },
    commentBtn:{
       borderRadius:responsiveHeight(1),
       backgroundColor:'#d75033',
       width:responsiveHeight(5),
       height:responsiveHeight(4),
       marginBottom:responsiveHeight(0.5)
    },
    commentText:{
        fontSize:responsiveFontSize(2),
        fontWeight:'bold'
    }
});