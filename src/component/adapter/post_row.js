import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import FitImage from 'react-native-fit-image';
import {COLOR, ThemeProvider, Card} from 'react-native-material-ui';
import { Icon,Button,Divider,Row,Col,Grid } from 'react-native-elements';
import {responsiveHeight, responsiveWidth, responsiveFontSize} from 'react-native-responsive-dimensions';

const POSTROW = (props) => (
    <Card>
        <FitImage
            source={{uri: 'http://192.168.43.3:4300/images/second.png'}}
            resizeMode="stretch"
            style={styles.fitImageWithSize}
        />
        <View style={styles.footerContainer}>
            <Text>{`${props.post_title}`}</Text>
            <View style={styles.footerRightContent}>
                <Icon
                    name='favorite'
                    type='material'
                    color='#000'
                    size={responsiveHeight(3)}
                    style={styles.footerRightItems}
                />
                <Text style={styles.footerRightItems}>0</Text>

                <Icon
                    name='favorite'
                    type='material'
                    color='#000'
                    size={responsiveHeight(3)}
                    style={styles.footerRightItems}
                />
                <Text style={styles.footerRightItems}>0</Text>

            </View>
        </View>
    </Card>
);

const styles = StyleSheet.create({
    fitImageWithSize: {
        height: responsiveHeight(30),
        width: responsiveWidth(100),
        padding: responsiveHeight(5)
    },
    footerContainer: {
        padding: responsiveHeight(2),
        width: responsiveWidth(100),
        display: 'flex',
        flexDirection: 'row'
    },
    footerRightContent: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        paddingRight: responsiveWidth(5)
    },
    footerRightItems:{
        paddingRight:responsiveWidth(2)
    }
});