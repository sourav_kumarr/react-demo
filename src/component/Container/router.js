/**
 * Created by sourav on 11/14/2017.
 */
import React from 'react';
import {TabNavigator,StackNavigator} from 'react-navigation';
import Reccommends from '../Pages/RecommendsView';
import Friends from '../Pages/Friends';
import Posts from '../Pages/Posts';
import PostsDescription from '../Pages/PostDescription';


export const PostStack = StackNavigator({
    Posts: {
        screen: Posts
    },
    PostDesc: {
        screen: PostsDescription

    }
},
{ navigationOptions: {
    header:false, //hide header if not needed so whole screen slide
}
}

);


export const Tabs = TabNavigator({
    Friend:{
        screen:Friends
    },
    Posts:{
        screen:PostStack
    }

},{
   tabBarPosition:'bottom',
   swipeEnabled:true,
   tabBarOptions:{
       style: {
           backgroundColor: '#d75033',
       },
       indicatorStyle: {
           backgroundColor: 'white'
       }
   }
});
