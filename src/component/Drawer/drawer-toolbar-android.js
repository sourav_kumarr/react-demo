import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    StatusBar,
    View,
    Alert
} from 'react-native';
import { COLOR, ThemeProvider, Toolbar, Drawer, Avatar } from 'react-native-material-ui';
import Container from '../Container';
import * as Globals from '../Helper/Globals';

let SharedPreferences = require('react-native-shared-preferences');


const uiTheme = {
    palette: {
        primaryColor: '#d75033',
        accentColor: COLOR.pink500,
      },
    toolbar: {
        container: {
            height: 70,
            paddingTop: 20,
          },
      },
      avatar: {
          container: {
              backgroundColor: '#d75033'
          }
      }
};

export default class DrawerMenu extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
        active: 'people',
        user_name:'Swati Sharma',
        user_email:'sksh3527@gmail.com',
        user_name_char:'S'
      };
  }

    //this method will not get called first time
    componentWillReceiveProps(newProps) {
        
        /*SharedPreferences.getItem(Globals.USER_NAME, function(value){
            console.log(value);
            if(value) {
                this.setState({user_name:value});
            }
        });

        SharedPreferences.getItem(Globals.USER_EMAIL, function(value){
            console.log(value);
            if(value) {
                this.setState({user_email:value});
            }
        });*/

    }

   componentDidMount() {

        console.log('componentDidMount is called');
        // this.setState({user_name:'Hello mr. lobo'});
       let state = this;
       let data = {};
       SharedPreferences.getItem(Globals.USER_NAME, function(value){
           console.log(value);
           if(value) {
               state.setState({user_name:value,user_name_char:value.charAt(0)});
           }
       });

       SharedPreferences.getItem(Globals.USER_EMAIL, function(value){
           console.log(value);
           if(value) {
               state.setState({user_email:value});
           }
       });


   }
  _setInfoActive() {
    this.setState({ active: 'info' });
  }

  render() {
    return (
        <ThemeProvider uiTheme={uiTheme}>
                <Container>
                    <StatusBar backgroundColor="rgba(0, 0, 0, 0.2)" translucent />
                    <Toolbar
                    leftElement="arrow-back"
                    onLeftElementPress={() => this.props.navigation.navigate('DrawerClose')}
                    centerElement="Menu"
                />
                    <View style={styles.container}>
                        <Drawer>
                            <Drawer.Header >
                                <Drawer.Header.Account
                                style={{ 
                                    container: { backgroundColor: '#fafafa' },
                                }}
                                avatar={<Avatar text={this.state.user_name_char} />}

                                footer={{
                                    dense: true,
                                    centerElement: {
                                        primaryText: this.state.user_name,
                                        secondaryText: this.state.user_email,
                                    },
                                    rightElement: 'arrow-drop-down',
                                  }}
                            />
                            </Drawer.Header>
                            <Drawer.Section
                            style={{
                                label: {color: '#0000ff'}
                            }}
                            divider
                            items={[
                                {
                                    icon: 'notifications', value: 'Notifications',
                                    active: this.state.active === 'notifications',
                                    onPress: () => {
                                        this.setState({ active: 'notifications' });
                                        this.props.navigation.navigate('Notification');
                                      },
                                  },
                                {
                                    icon: 'chat', value: 'Messages',
                                    active: this.state.active === 'messages',
                                    onPress: () => {
                                        this.setState({ active: 'messages' });
                                        this.props.navigation.navigate('Messages');
                                      },
                                  },
                            ]}
                        />
                            <Drawer.Section
                            title="Personal"
                            items={[
                                {
                                    icon: 'settings', value: 'Setting',
                                    active: this.state.active === 'settings',
                                    onPress: () => {
                                        this.setState({ active: 'settings' });
                                        this.props.navigation.navigate('Settings');
                                    },
                                },
                                {
                                    icon: 'info', value: 'About US',
                                    active: this.state.active === 'aboutus',
                                    onPress: () => {
                                        this.setState({ active: 'aboutus' });

                                        //this.props.navigation.navigate('DrawerClose');
                                        this.props.navigation.navigate('About');
                                      },
                                  },
                                {
                                    icon: 'call', value: 'Contact US',
                                    active: this.state.active === 'contact',
                                    onPress: () => {
                                        this.setState({ active: 'contact' });
                                        this.props.navigation.navigate('Contact');
                                      },
                                  },
                            ]}
                        />
                        </Drawer>
                    </View>
                </Container>
            </ThemeProvider>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
      },
    header: {
        backgroundColor: '#455A64',
      },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
      },
  });
