module.exports = {

    BASE_URL: 'https://codefish-appy.herokuapp.com/',
    LOGIN_URL :'login/',
    REGISTER_URL :'users/register',
    POSTS_URL :'posts/',
    COMMENT_URL :'comments/',
    POST_COMMENT_URL :'comment/',
    POST_LIKE_URL :'like/',
    FRIEND_URL:'users/friends',
    IMAGE_URL: 'https://codefish-appy.herokuapp.com/images/',
    OTP_URL: 'users/otp',
    VERIFY_OTP_URL: 'users/otpVerify',


    STATUS:'Status',
    MESSAGE:'Message',
    SUCCESS:'Success',
    LOGGED_IN:'logged_in',
    USER_NAME:'user_name',
    USER_EMAIL:'user_email',
    USER_ID:'user_id',

    COLOR: {
        ORANGE: '#C50',
        DARKBLUE: '#0F3274',
        LIGHTBLUE: '#6EA8DA',
        DARKGRAY: '#999',
    },
};