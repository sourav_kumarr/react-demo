import React, { Component } from 'react';
import {
    AppRegistry
} from 'react-native';

import Login from './src/component/Pages/Login';
import Secured from './src/component/Pages/Secured';

import { DrawerNavigator, StackNavigator } from 'react-navigation';

const Root = StackNavigator({
    Login:{
        screen:Login
    },
    Secured:{
        screen:Secured
    },
}, {
    headerMode: 'none'
});

AppRegistry.registerComponent("AwesomeProject" , () => Root );